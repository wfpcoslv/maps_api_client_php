# Beneficiary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beneficiary_group** | [**\Swagger\Client\Model\BeneficiaryGroup**](BeneficiaryGroup.md) |  | [optional] 
**beneficiary_unique_id** | **string** |  | [optional] 
**birth_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**cards** | [**\Swagger\Client\Model\Card[]**](Card.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**delegate_document_id** | **string** |  | [optional] 
**delegate_first_name** | **string** |  | [optional] 
**delegate_last_name** | **string** |  | [optional] 
**delegate_relationship** | **string** |  | [optional] 
**deleted** | **bool** |  | 
**document_id** | **string** |  | [optional] 
**first_name** | **string** |  | 
**gender** | **string** |  | 
**id** | **int** |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_name** | **string** |  | 
**nutrimiles_id** | **string** |  | [optional] 
**registration_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**version** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


