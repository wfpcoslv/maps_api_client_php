# CustomRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | 
**features** | [**\Swagger\Client\Model\Authority[]**](Authority.md) |  | [optional] 
**id** | **int** |  | [optional] 
**label** | **string** |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**organization** | [**\Swagger\Client\Model\Organization**](Organization.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


