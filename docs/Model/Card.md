# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | 
**id** | **int** |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**uuid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


