# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accuracy** | **float** |  | [optional] 
**beneficiary** | [**\Swagger\Client\Model\Beneficiary**](Beneficiary.md) |  | [optional] 
**code** | **int** |  | [optional] 
**coresponsibility** | [**\Swagger\Client\Model\Coresponsibility**](Coresponsibility.md) |  | [optional] 
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**data** | **string** |  | [optional] 
**deleted** | **bool** |  | 
**device_famoco_id** | **string** |  | [optional] 
**device_imei** | **string** |  | [optional] 
**device_model_name** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**latitude** | **float** |  | [optional] 
**location_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**longitude** | **float** |  | [optional] 
**transaction_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


