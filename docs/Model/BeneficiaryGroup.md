# BeneficiaryGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** |  | [optional] 
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | 
**description** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**name** | **string** |  | 
**organization** | [**\Swagger\Client\Model\Organization**](Organization.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


