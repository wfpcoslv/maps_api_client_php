# Coresponsibility

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beneficiary_groups** | [**\Swagger\Client\Model\BeneficiaryGroup[]**](BeneficiaryGroup.md) |  | [optional] 
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | 
**id** | **int** |  | [optional] 
**label** | **string** |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**programme** | [**\Swagger\Client\Model\Programme**](Programme.md) |  | [optional] 
**template_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


