# Programme

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **string** |  | [optional] 
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | 
**description** | **string** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id** | **int** |  | [optional] 
**label** | **string** |  | 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**organization** | [**\Swagger\Client\Model\Organization**](Organization.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


