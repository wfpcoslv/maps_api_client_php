# DataFilterDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beneficiary_id** | **int** |  | [optional] 
**beneficiary_name** | **string** |  | [optional] 
**coresponsibility_id** | **int** |  | [optional] 
**coresponsibility_label** | **string** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**group_id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**organization_id** | **int** |  | 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


