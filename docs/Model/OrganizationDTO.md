# OrganizationDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**about_message** | **string** |  | [optional] 
**comment** | **string** |  | [optional] 
**contact_email** | **string** |  | [optional] 
**contact_name** | **string** |  | [optional] 
**contact_phone** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**deleted** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**label** | **string** |  | 
**logo_device** | **string[]** |  | [optional] 
**logo_device_content_type** | **string** |  | [optional] 
**logo_device_url** | **string** |  | [optional] 
**logo_web** | **string[]** |  | [optional] 
**logo_web_content_type** | **string** |  | [optional] 
**logo_web_url** | **string** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status** | **bool** |  | [optional] 
**theme** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


