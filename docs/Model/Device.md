# Device

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_current_version** | **string** |  | [optional] 
**creation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**deleted** | **bool** |  | 
**id** | **int** |  | [optional] 
**last_connection_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_modification_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**location_accuracy** | **float** |  | [optional] 
**location_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**location_latitude** | **float** |  | [optional] 
**location_longitude** | **float** |  | [optional] 
**model_name** | **string** |  | [optional] 
**uuid** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


