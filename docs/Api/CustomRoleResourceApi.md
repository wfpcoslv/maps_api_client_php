# Swagger\Client\CustomRoleResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomRoleUsingPOST**](CustomRoleResourceApi.md#createCustomRoleUsingPOST) | **POST** /api/custom-roles | createCustomRole
[**deleteCustomRoleUsingDELETE**](CustomRoleResourceApi.md#deleteCustomRoleUsingDELETE) | **DELETE** /api/custom-roles/{id} | deleteCustomRole
[**getAllCustomRolesUsingGET**](CustomRoleResourceApi.md#getAllCustomRolesUsingGET) | **GET** /api/custom-roles | getAllCustomRoles
[**getCustomRoleUsingGET**](CustomRoleResourceApi.md#getCustomRoleUsingGET) | **GET** /api/custom-roles/{id} | getCustomRole
[**getCustomRolesByOrganizationUsingGET**](CustomRoleResourceApi.md#getCustomRolesByOrganizationUsingGET) | **GET** /api/custom-roles/organization/{id} | getCustomRolesByOrganization
[**updateCustomRoleUsingPUT**](CustomRoleResourceApi.md#updateCustomRoleUsingPUT) | **PUT** /api/custom-roles | updateCustomRole


# **createCustomRoleUsingPOST**
> \Swagger\Client\Model\CustomRole createCustomRoleUsingPOST($custom_role)

createCustomRole

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CustomRoleResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$custom_role = new \Swagger\Client\Model\CustomRole(); // \Swagger\Client\Model\CustomRole | customRole

try {
    $result = $apiInstance->createCustomRoleUsingPOST($custom_role);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomRoleResourceApi->createCustomRoleUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **custom_role** | [**\Swagger\Client\Model\CustomRole**](../Model/CustomRole.md)| customRole |

### Return type

[**\Swagger\Client\Model\CustomRole**](../Model/CustomRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCustomRoleUsingDELETE**
> deleteCustomRoleUsingDELETE($id)

deleteCustomRole

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CustomRoleResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteCustomRoleUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling CustomRoleResourceApi->deleteCustomRoleUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllCustomRolesUsingGET**
> \Swagger\Client\Model\CustomRole[] getAllCustomRolesUsingGET()

getAllCustomRoles

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CustomRoleResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllCustomRolesUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomRoleResourceApi->getAllCustomRolesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CustomRole[]**](../Model/CustomRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomRoleUsingGET**
> \Swagger\Client\Model\CustomRole getCustomRoleUsingGET($id)

getCustomRole

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CustomRoleResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getCustomRoleUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomRoleResourceApi->getCustomRoleUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\CustomRole**](../Model/CustomRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomRolesByOrganizationUsingGET**
> \Swagger\Client\Model\CustomRole[] getCustomRolesByOrganizationUsingGET($id)

getCustomRolesByOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CustomRoleResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getCustomRolesByOrganizationUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomRoleResourceApi->getCustomRolesByOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\CustomRole[]**](../Model/CustomRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCustomRoleUsingPUT**
> \Swagger\Client\Model\CustomRole updateCustomRoleUsingPUT($custom_role)

updateCustomRole

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CustomRoleResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$custom_role = new \Swagger\Client\Model\CustomRole(); // \Swagger\Client\Model\CustomRole | customRole

try {
    $result = $apiInstance->updateCustomRoleUsingPUT($custom_role);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomRoleResourceApi->updateCustomRoleUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **custom_role** | [**\Swagger\Client\Model\CustomRole**](../Model/CustomRole.md)| customRole |

### Return type

[**\Swagger\Client\Model\CustomRole**](../Model/CustomRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

