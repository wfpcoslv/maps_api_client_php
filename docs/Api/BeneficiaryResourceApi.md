# Swagger\Client\BeneficiaryResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countBeneficiariesByOrganizationUsingGET**](BeneficiaryResourceApi.md#countBeneficiariesByOrganizationUsingGET) | **GET** /api/beneficiaries/number | countBeneficiariesByOrganization
[**createBeneficiaryUsingPOST**](BeneficiaryResourceApi.md#createBeneficiaryUsingPOST) | **POST** /api/beneficiaries | createBeneficiary
[**deleteBeneficiaryUsingDELETE**](BeneficiaryResourceApi.md#deleteBeneficiaryUsingDELETE) | **DELETE** /api/beneficiaries/{id} | deleteBeneficiary
[**getAllBeneficiariesUsingGET**](BeneficiaryResourceApi.md#getAllBeneficiariesUsingGET) | **GET** /api/beneficiaries | getAllBeneficiaries
[**getBeneficiariesByDataFilterDTOUsingPOST**](BeneficiaryResourceApi.md#getBeneficiariesByDataFilterDTOUsingPOST) | **POST** /api/beneficiaries/organization | getBeneficiariesByDataFilterDTO
[**getBeneficiaryUsingGET**](BeneficiaryResourceApi.md#getBeneficiaryUsingGET) | **GET** /api/beneficiaries/{id} | getBeneficiary
[**updateBeneficiaryUsingPUT**](BeneficiaryResourceApi.md#updateBeneficiaryUsingPUT) | **PUT** /api/beneficiaries | updateBeneficiary


# **countBeneficiariesByOrganizationUsingGET**
> int countBeneficiariesByOrganizationUsingGET($organization_id)

countBeneficiariesByOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_id = 789; // int | organization_id

try {
    $result = $apiInstance->countBeneficiariesByOrganizationUsingGET($organization_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->countBeneficiariesByOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | **int**| organization_id |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createBeneficiaryUsingPOST**
> \Swagger\Client\Model\Beneficiary createBeneficiaryUsingPOST($beneficiary)

createBeneficiary

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$beneficiary = new \Swagger\Client\Model\Beneficiary(); // \Swagger\Client\Model\Beneficiary | beneficiary

try {
    $result = $apiInstance->createBeneficiaryUsingPOST($beneficiary);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->createBeneficiaryUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **beneficiary** | [**\Swagger\Client\Model\Beneficiary**](../Model/Beneficiary.md)| beneficiary |

### Return type

[**\Swagger\Client\Model\Beneficiary**](../Model/Beneficiary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBeneficiaryUsingDELETE**
> deleteBeneficiaryUsingDELETE($id)

deleteBeneficiary

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteBeneficiaryUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->deleteBeneficiaryUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllBeneficiariesUsingGET**
> \Swagger\Client\Model\Beneficiary[] getAllBeneficiariesUsingGET($organization_id, $programme_id, $first_name, $last_name, $document_id, $beneficiary_group_id, $birth_date)

getAllBeneficiaries

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_id = 789; // int | organization_id
$programme_id = 789; // int | programme_id
$first_name = "first_name_example"; // string | first_name
$last_name = "last_name_example"; // string | last_name
$document_id = "document_id_example"; // string | document_id
$beneficiary_group_id = 789; // int | beneficiary_group_id
$birth_date = new \DateTime("2013-10-20"); // \DateTime | birth_date

try {
    $result = $apiInstance->getAllBeneficiariesUsingGET($organization_id, $programme_id, $first_name, $last_name, $document_id, $beneficiary_group_id, $birth_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->getAllBeneficiariesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | **int**| organization_id | [optional]
 **programme_id** | **int**| programme_id | [optional]
 **first_name** | **string**| first_name | [optional]
 **last_name** | **string**| last_name | [optional]
 **document_id** | **string**| document_id | [optional]
 **beneficiary_group_id** | **int**| beneficiary_group_id | [optional]
 **birth_date** | **\DateTime**| birth_date | [optional]

### Return type

[**\Swagger\Client\Model\Beneficiary[]**](../Model/Beneficiary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBeneficiariesByDataFilterDTOUsingPOST**
> \Swagger\Client\Model\Beneficiary[] getBeneficiariesByDataFilterDTOUsingPOST($data_filter_dto)

getBeneficiariesByDataFilterDTO

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$data_filter_dto = new \Swagger\Client\Model\DataFilterDTO(); // \Swagger\Client\Model\DataFilterDTO | dataFilterDTO

try {
    $result = $apiInstance->getBeneficiariesByDataFilterDTOUsingPOST($data_filter_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->getBeneficiariesByDataFilterDTOUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_filter_dto** | [**\Swagger\Client\Model\DataFilterDTO**](../Model/DataFilterDTO.md)| dataFilterDTO |

### Return type

[**\Swagger\Client\Model\Beneficiary[]**](../Model/Beneficiary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBeneficiaryUsingGET**
> \Swagger\Client\Model\Beneficiary getBeneficiaryUsingGET($id)

getBeneficiary

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getBeneficiaryUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->getBeneficiaryUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Beneficiary**](../Model/Beneficiary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBeneficiaryUsingPUT**
> \Swagger\Client\Model\Beneficiary updateBeneficiaryUsingPUT($beneficiary)

updateBeneficiary

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$beneficiary = new \Swagger\Client\Model\Beneficiary(); // \Swagger\Client\Model\Beneficiary | beneficiary

try {
    $result = $apiInstance->updateBeneficiaryUsingPUT($beneficiary);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryResourceApi->updateBeneficiaryUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **beneficiary** | [**\Swagger\Client\Model\Beneficiary**](../Model/Beneficiary.md)| beneficiary |

### Return type

[**\Swagger\Client\Model\Beneficiary**](../Model/Beneficiary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

