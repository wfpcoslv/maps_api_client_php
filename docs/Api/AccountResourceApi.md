# Swagger\Client\AccountResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateAccountAndSetPwdUsingPOST**](AccountResourceApi.md#activateAccountAndSetPwdUsingPOST) | **POST** /api/activate | activateAccountAndSetPwd
[**activateAccountUsingGET**](AccountResourceApi.md#activateAccountUsingGET) | **GET** /api/activate | activateAccount
[**changePasswordUsingPOST**](AccountResourceApi.md#changePasswordUsingPOST) | **POST** /api/account/change_password | changePassword
[**createAdminUsingPOST**](AccountResourceApi.md#createAdminUsingPOST) | **POST** /api/register/admin | createAdmin
[**createOrgAdminUsingPOST**](AccountResourceApi.md#createOrgAdminUsingPOST) | **POST** /api/register/org_admin | createOrgAdmin
[**createUserUsingPOST**](AccountResourceApi.md#createUserUsingPOST) | **POST** /api/register/user | createUser
[**finishPasswordResetUsingPOST**](AccountResourceApi.md#finishPasswordResetUsingPOST) | **POST** /api/account/reset_password/finish | finishPasswordReset
[**getAccountUsingGET**](AccountResourceApi.md#getAccountUsingGET) | **GET** /api/account | getAccount
[**isAuthenticatedUsingGET**](AccountResourceApi.md#isAuthenticatedUsingGET) | **GET** /api/authenticate | isAuthenticated
[**registerAccountUsingPOST**](AccountResourceApi.md#registerAccountUsingPOST) | **POST** /api/register | registerAccount
[**requestPasswordResetUsingPOST**](AccountResourceApi.md#requestPasswordResetUsingPOST) | **POST** /api/account/reset_password/init | requestPasswordReset
[**saveAccountUsingPOST**](AccountResourceApi.md#saveAccountUsingPOST) | **POST** /api/account | saveAccount


# **activateAccountAndSetPwdUsingPOST**
> string activateAccountAndSetPwdUsingPOST($key_and_password)

activateAccountAndSetPwd

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$key_and_password = new \Swagger\Client\Model\KeyAndPasswordVM(); // \Swagger\Client\Model\KeyAndPasswordVM | keyAndPassword

try {
    $result = $apiInstance->activateAccountAndSetPwdUsingPOST($key_and_password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->activateAccountAndSetPwdUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_and_password** | [**\Swagger\Client\Model\KeyAndPasswordVM**](../Model/KeyAndPasswordVM.md)| keyAndPassword |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **activateAccountUsingGET**
> string activateAccountUsingGET($key)

activateAccount

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$key = "key_example"; // string | key

try {
    $result = $apiInstance->activateAccountUsingGET($key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->activateAccountUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **string**| key |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changePasswordUsingPOST**
> \Swagger\Client\Model\ResponseEntity changePasswordUsingPOST($password)

changePassword

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$password = "password_example"; // string | password

try {
    $result = $apiInstance->changePasswordUsingPOST($password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->changePasswordUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | **string**| password |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createAdminUsingPOST**
> \Swagger\Client\Model\ResponseEntity createAdminUsingPOST($managed_user_vm)

createAdmin

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$managed_user_vm = new \Swagger\Client\Model\UserDTO(); // \Swagger\Client\Model\UserDTO | managedUserVM

try {
    $result = $apiInstance->createAdminUsingPOST($managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->createAdminUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managed_user_vm** | [**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)| managedUserVM |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createOrgAdminUsingPOST**
> \Swagger\Client\Model\ResponseEntity createOrgAdminUsingPOST($managed_user_vm)

createOrgAdmin

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$managed_user_vm = new \Swagger\Client\Model\UserDTO(); // \Swagger\Client\Model\UserDTO | managedUserVM

try {
    $result = $apiInstance->createOrgAdminUsingPOST($managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->createOrgAdminUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managed_user_vm** | [**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)| managedUserVM |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserUsingPOST**
> \Swagger\Client\Model\ResponseEntity createUserUsingPOST($managed_user_vm)

createUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$managed_user_vm = new \Swagger\Client\Model\UserDTO(); // \Swagger\Client\Model\UserDTO | managedUserVM

try {
    $result = $apiInstance->createUserUsingPOST($managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->createUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managed_user_vm** | [**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)| managedUserVM |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **finishPasswordResetUsingPOST**
> string finishPasswordResetUsingPOST($key_and_password)

finishPasswordReset

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$key_and_password = new \Swagger\Client\Model\KeyAndPasswordVM(); // \Swagger\Client\Model\KeyAndPasswordVM | keyAndPassword

try {
    $result = $apiInstance->finishPasswordResetUsingPOST($key_and_password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->finishPasswordResetUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_and_password** | [**\Swagger\Client\Model\KeyAndPasswordVM**](../Model/KeyAndPasswordVM.md)| keyAndPassword |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccountUsingGET**
> \Swagger\Client\Model\UserDTO getAccountUsingGET()

getAccount

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAccountUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->getAccountUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **isAuthenticatedUsingGET**
> string isAuthenticatedUsingGET()

isAuthenticated

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->isAuthenticatedUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->isAuthenticatedUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerAccountUsingPOST**
> \Swagger\Client\Model\ResponseEntity registerAccountUsingPOST($managed_user_vm)

registerAccount

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$managed_user_vm = new \Swagger\Client\Model\UserDTO(); // \Swagger\Client\Model\UserDTO | managedUserVM

try {
    $result = $apiInstance->registerAccountUsingPOST($managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->registerAccountUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managed_user_vm** | [**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)| managedUserVM |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **requestPasswordResetUsingPOST**
> \Swagger\Client\Model\ResponseEntity requestPasswordResetUsingPOST($login)

requestPasswordReset

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login = "login_example"; // string | login

try {
    $result = $apiInstance->requestPasswordResetUsingPOST($login);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->requestPasswordResetUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **string**| login |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **saveAccountUsingPOST**
> \Swagger\Client\Model\ResponseEntity saveAccountUsingPOST($user_dto)

saveAccount

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AccountResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user_dto = new \Swagger\Client\Model\UserDTO(); // \Swagger\Client\Model\UserDTO | userDTO

try {
    $result = $apiInstance->saveAccountUsingPOST($user_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountResourceApi->saveAccountUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_dto** | [**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)| userDTO |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

