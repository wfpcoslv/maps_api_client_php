# Swagger\Client\DeviceLogResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDeviceLogUsingPOST**](DeviceLogResourceApi.md#createDeviceLogUsingPOST) | **POST** /api/device-logs | createDeviceLog
[**deleteDeviceLogUsingDELETE**](DeviceLogResourceApi.md#deleteDeviceLogUsingDELETE) | **DELETE** /api/device-logs/{id} | deleteDeviceLog
[**getAllDeviceLogsUsingGET**](DeviceLogResourceApi.md#getAllDeviceLogsUsingGET) | **GET** /api/device-logs | getAllDeviceLogs
[**getDeviceLogUsingGET**](DeviceLogResourceApi.md#getDeviceLogUsingGET) | **GET** /api/device-logs/{id} | getDeviceLog
[**updateDeviceLogUsingPUT**](DeviceLogResourceApi.md#updateDeviceLogUsingPUT) | **PUT** /api/device-logs | updateDeviceLog


# **createDeviceLogUsingPOST**
> \Swagger\Client\Model\DeviceLog createDeviceLogUsingPOST($device_log)

createDeviceLog

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceLogResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_log = new \Swagger\Client\Model\DeviceLog(); // \Swagger\Client\Model\DeviceLog | deviceLog

try {
    $result = $apiInstance->createDeviceLogUsingPOST($device_log);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceLogResourceApi->createDeviceLogUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_log** | [**\Swagger\Client\Model\DeviceLog**](../Model/DeviceLog.md)| deviceLog |

### Return type

[**\Swagger\Client\Model\DeviceLog**](../Model/DeviceLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDeviceLogUsingDELETE**
> deleteDeviceLogUsingDELETE($id)

deleteDeviceLog

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceLogResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteDeviceLogUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling DeviceLogResourceApi->deleteDeviceLogUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllDeviceLogsUsingGET**
> \Swagger\Client\Model\DeviceLog[] getAllDeviceLogsUsingGET()

getAllDeviceLogs

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceLogResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllDeviceLogsUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceLogResourceApi->getAllDeviceLogsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\DeviceLog[]**](../Model/DeviceLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDeviceLogUsingGET**
> \Swagger\Client\Model\DeviceLog getDeviceLogUsingGET($id)

getDeviceLog

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceLogResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getDeviceLogUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceLogResourceApi->getDeviceLogUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\DeviceLog**](../Model/DeviceLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDeviceLogUsingPUT**
> \Swagger\Client\Model\DeviceLog updateDeviceLogUsingPUT($device_log)

updateDeviceLog

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceLogResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_log = new \Swagger\Client\Model\DeviceLog(); // \Swagger\Client\Model\DeviceLog | deviceLog

try {
    $result = $apiInstance->updateDeviceLogUsingPUT($device_log);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceLogResourceApi->updateDeviceLogUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_log** | [**\Swagger\Client\Model\DeviceLog**](../Model/DeviceLog.md)| deviceLog |

### Return type

[**\Swagger\Client\Model\DeviceLog**](../Model/DeviceLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

