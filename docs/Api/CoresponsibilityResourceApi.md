# Swagger\Client\CoresponsibilityResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCoresponsibilityUsingPOST**](CoresponsibilityResourceApi.md#createCoresponsibilityUsingPOST) | **POST** /api/coresponsibilities | createCoresponsibility
[**deleteCoresponsibilityUsingDELETE**](CoresponsibilityResourceApi.md#deleteCoresponsibilityUsingDELETE) | **DELETE** /api/coresponsibilities/{id} | deleteCoresponsibility
[**getAllCoresponsibilitiesUsingGET**](CoresponsibilityResourceApi.md#getAllCoresponsibilitiesUsingGET) | **GET** /api/coresponsibilities | getAllCoresponsibilities
[**getCorespensabilityByOrganizationUsingGET**](CoresponsibilityResourceApi.md#getCorespensabilityByOrganizationUsingGET) | **GET** /api/coresponsibilities/organization/{id} | getCorespensabilityByOrganization
[**getCoresponsibilityUsingGET**](CoresponsibilityResourceApi.md#getCoresponsibilityUsingGET) | **GET** /api/coresponsibilities/{id} | getCoresponsibility
[**updateCoresponsibilityUsingPUT**](CoresponsibilityResourceApi.md#updateCoresponsibilityUsingPUT) | **PUT** /api/coresponsibilities | updateCoresponsibility


# **createCoresponsibilityUsingPOST**
> \Swagger\Client\Model\Coresponsibility createCoresponsibilityUsingPOST($coresponsibility, $file)

createCoresponsibility

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CoresponsibilityResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$coresponsibility = new \Swagger\Client\Model\\Swagger\Client\Model\Object(); // \Swagger\Client\Model\Object | coresponsibility
$file = "/path/to/file.txt"; // \SplFileObject | file

try {
    $result = $apiInstance->createCoresponsibilityUsingPOST($coresponsibility, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoresponsibilityResourceApi->createCoresponsibilityUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **coresponsibility** | [**\Swagger\Client\Model\Object**](../Model/.md)| coresponsibility |
 **file** | **\SplFileObject**| file | [optional]

### Return type

[**\Swagger\Client\Model\Coresponsibility**](../Model/Coresponsibility.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCoresponsibilityUsingDELETE**
> deleteCoresponsibilityUsingDELETE($id)

deleteCoresponsibility

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CoresponsibilityResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteCoresponsibilityUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling CoresponsibilityResourceApi->deleteCoresponsibilityUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllCoresponsibilitiesUsingGET**
> \Swagger\Client\Model\Coresponsibility[] getAllCoresponsibilitiesUsingGET($organization_id, $programme_id)

getAllCoresponsibilities

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CoresponsibilityResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_id = 789; // int | organization_id
$programme_id = 789; // int | programme_id

try {
    $result = $apiInstance->getAllCoresponsibilitiesUsingGET($organization_id, $programme_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoresponsibilityResourceApi->getAllCoresponsibilitiesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | **int**| organization_id | [optional]
 **programme_id** | **int**| programme_id | [optional]

### Return type

[**\Swagger\Client\Model\Coresponsibility[]**](../Model/Coresponsibility.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCorespensabilityByOrganizationUsingGET**
> \Swagger\Client\Model\Coresponsibility[] getCorespensabilityByOrganizationUsingGET($id)

getCorespensabilityByOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CoresponsibilityResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getCorespensabilityByOrganizationUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoresponsibilityResourceApi->getCorespensabilityByOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Coresponsibility[]**](../Model/Coresponsibility.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCoresponsibilityUsingGET**
> \Swagger\Client\Model\Coresponsibility getCoresponsibilityUsingGET($id)

getCoresponsibility

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CoresponsibilityResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getCoresponsibilityUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoresponsibilityResourceApi->getCoresponsibilityUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Coresponsibility**](../Model/Coresponsibility.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCoresponsibilityUsingPUT**
> \Swagger\Client\Model\Coresponsibility updateCoresponsibilityUsingPUT($coresponsibility)

updateCoresponsibility

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CoresponsibilityResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$coresponsibility = new \Swagger\Client\Model\Coresponsibility(); // \Swagger\Client\Model\Coresponsibility | coresponsibility

try {
    $result = $apiInstance->updateCoresponsibilityUsingPUT($coresponsibility);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoresponsibilityResourceApi->updateCoresponsibilityUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **coresponsibility** | [**\Swagger\Client\Model\Coresponsibility**](../Model/Coresponsibility.md)| coresponsibility |

### Return type

[**\Swagger\Client\Model\Coresponsibility**](../Model/Coresponsibility.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

