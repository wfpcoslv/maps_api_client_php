# Swagger\Client\BeneficiaryGroupResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBeneficiaryGroupUsingPOST**](BeneficiaryGroupResourceApi.md#createBeneficiaryGroupUsingPOST) | **POST** /api/beneficiary-groups | createBeneficiaryGroup
[**deleteBeneficiaryGroupUsingDELETE**](BeneficiaryGroupResourceApi.md#deleteBeneficiaryGroupUsingDELETE) | **DELETE** /api/beneficiary-groups/{id} | deleteBeneficiaryGroup
[**getAllBeneficiaryGroupsUsingGET**](BeneficiaryGroupResourceApi.md#getAllBeneficiaryGroupsUsingGET) | **GET** /api/beneficiary-groups | getAllBeneficiaryGroups
[**getBeneficiaryGroupByOrganizationUsingGET**](BeneficiaryGroupResourceApi.md#getBeneficiaryGroupByOrganizationUsingGET) | **GET** /api/beneficiary-groups/organization/{id} | getBeneficiaryGroupByOrganization
[**getBeneficiaryGroupUsingGET**](BeneficiaryGroupResourceApi.md#getBeneficiaryGroupUsingGET) | **GET** /api/beneficiary-groups/{id} | getBeneficiaryGroup
[**updateBeneficiaryGroupUsingPUT**](BeneficiaryGroupResourceApi.md#updateBeneficiaryGroupUsingPUT) | **PUT** /api/beneficiary-groups | updateBeneficiaryGroup


# **createBeneficiaryGroupUsingPOST**
> \Swagger\Client\Model\BeneficiaryGroup createBeneficiaryGroupUsingPOST($beneficiary_group)

createBeneficiaryGroup

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryGroupResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$beneficiary_group = new \Swagger\Client\Model\BeneficiaryGroup(); // \Swagger\Client\Model\BeneficiaryGroup | beneficiaryGroup

try {
    $result = $apiInstance->createBeneficiaryGroupUsingPOST($beneficiary_group);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryGroupResourceApi->createBeneficiaryGroupUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **beneficiary_group** | [**\Swagger\Client\Model\BeneficiaryGroup**](../Model/BeneficiaryGroup.md)| beneficiaryGroup |

### Return type

[**\Swagger\Client\Model\BeneficiaryGroup**](../Model/BeneficiaryGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBeneficiaryGroupUsingDELETE**
> deleteBeneficiaryGroupUsingDELETE($id)

deleteBeneficiaryGroup

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryGroupResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteBeneficiaryGroupUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryGroupResourceApi->deleteBeneficiaryGroupUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllBeneficiaryGroupsUsingGET**
> \Swagger\Client\Model\BeneficiaryGroup[] getAllBeneficiaryGroupsUsingGET()

getAllBeneficiaryGroups

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryGroupResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllBeneficiaryGroupsUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryGroupResourceApi->getAllBeneficiaryGroupsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\BeneficiaryGroup[]**](../Model/BeneficiaryGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBeneficiaryGroupByOrganizationUsingGET**
> \Swagger\Client\Model\BeneficiaryGroup[] getBeneficiaryGroupByOrganizationUsingGET($id)

getBeneficiaryGroupByOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryGroupResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getBeneficiaryGroupByOrganizationUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryGroupResourceApi->getBeneficiaryGroupByOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\BeneficiaryGroup[]**](../Model/BeneficiaryGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBeneficiaryGroupUsingGET**
> \Swagger\Client\Model\BeneficiaryGroup getBeneficiaryGroupUsingGET($id)

getBeneficiaryGroup

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryGroupResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getBeneficiaryGroupUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryGroupResourceApi->getBeneficiaryGroupUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\BeneficiaryGroup**](../Model/BeneficiaryGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBeneficiaryGroupUsingPUT**
> \Swagger\Client\Model\BeneficiaryGroup updateBeneficiaryGroupUsingPUT($beneficiary_group)

updateBeneficiaryGroup

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BeneficiaryGroupResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$beneficiary_group = new \Swagger\Client\Model\BeneficiaryGroup(); // \Swagger\Client\Model\BeneficiaryGroup | beneficiaryGroup

try {
    $result = $apiInstance->updateBeneficiaryGroupUsingPUT($beneficiary_group);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BeneficiaryGroupResourceApi->updateBeneficiaryGroupUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **beneficiary_group** | [**\Swagger\Client\Model\BeneficiaryGroup**](../Model/BeneficiaryGroup.md)| beneficiaryGroup |

### Return type

[**\Swagger\Client\Model\BeneficiaryGroup**](../Model/BeneficiaryGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

