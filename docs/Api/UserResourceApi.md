# Swagger\Client\UserResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkLoginDispoUsingGET**](UserResourceApi.md#checkLoginDispoUsingGET) | **GET** /api/check/login/dispo/{login} | checkLoginDispo
[**createOrganizationUserUsingPOST**](UserResourceApi.md#createOrganizationUserUsingPOST) | **POST** /api/users/organization/{id} | createOrganizationUser
[**createUserUsingPOST1**](UserResourceApi.md#createUserUsingPOST1) | **POST** /api/users | createUser
[**deleteUserUsingDELETE**](UserResourceApi.md#deleteUserUsingDELETE) | **DELETE** /api/users/{login} | deleteUser
[**getAllUsersUsingGET**](UserResourceApi.md#getAllUsersUsingGET) | **GET** /api/users | getAllUsers
[**getOrganizationUsersUsingGET**](UserResourceApi.md#getOrganizationUsersUsingGET) | **GET** /api/users/organization/{id} | getOrganizationUsers
[**getUserUsingGET**](UserResourceApi.md#getUserUsingGET) | **GET** /api/users/{login} | getUser
[**updateUserUsingPUT**](UserResourceApi.md#updateUserUsingPUT) | **PUT** /api/users | updateUser


# **checkLoginDispoUsingGET**
> object checkLoginDispoUsingGET($login)

checkLoginDispo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login = "login_example"; // string | login

try {
    $result = $apiInstance->checkLoginDispoUsingGET($login);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->checkLoginDispoUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **string**| login |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createOrganizationUserUsingPOST**
> object createOrganizationUserUsingPOST($id, $managed_user_vm)

createOrganizationUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id
$managed_user_vm = new \Swagger\Client\Model\ManagedUserVM(); // \Swagger\Client\Model\ManagedUserVM | managedUserVM

try {
    $result = $apiInstance->createOrganizationUserUsingPOST($id, $managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->createOrganizationUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |
 **managed_user_vm** | [**\Swagger\Client\Model\ManagedUserVM**](../Model/ManagedUserVM.md)| managedUserVM |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserUsingPOST1**
> \Swagger\Client\Model\ResponseEntity createUserUsingPOST1($managed_user_vm)

createUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$managed_user_vm = new \Swagger\Client\Model\ManagedUserVM(); // \Swagger\Client\Model\ManagedUserVM | managedUserVM

try {
    $result = $apiInstance->createUserUsingPOST1($managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->createUserUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managed_user_vm** | [**\Swagger\Client\Model\ManagedUserVM**](../Model/ManagedUserVM.md)| managedUserVM |

### Return type

[**\Swagger\Client\Model\ResponseEntity**](../Model/ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserUsingDELETE**
> deleteUserUsingDELETE($login)

deleteUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login = "login_example"; // string | login

try {
    $apiInstance->deleteUserUsingDELETE($login);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->deleteUserUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **string**| login |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllUsersUsingGET**
> \Swagger\Client\Model\UserDTO[] getAllUsersUsingGET()

getAllUsers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllUsersUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->getAllUsersUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserDTO[]**](../Model/UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOrganizationUsersUsingGET**
> \Swagger\Client\Model\UserDTO[] getOrganizationUsersUsingGET($id)

getOrganizationUsers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getOrganizationUsersUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->getOrganizationUsersUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\UserDTO[]**](../Model/UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserUsingGET**
> \Swagger\Client\Model\UserDTO getUserUsingGET($login)

getUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login = "login_example"; // string | login

try {
    $result = $apiInstance->getUserUsingGET($login);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->getUserUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **string**| login |

### Return type

[**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUserUsingPUT**
> \Swagger\Client\Model\UserDTO updateUserUsingPUT($managed_user_vm)

updateUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$managed_user_vm = new \Swagger\Client\Model\ManagedUserVM(); // \Swagger\Client\Model\ManagedUserVM | managedUserVM

try {
    $result = $apiInstance->updateUserUsingPUT($managed_user_vm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserResourceApi->updateUserUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managed_user_vm** | [**\Swagger\Client\Model\ManagedUserVM**](../Model/ManagedUserVM.md)| managedUserVM |

### Return type

[**\Swagger\Client\Model\UserDTO**](../Model/UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

