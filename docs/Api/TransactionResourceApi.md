# Swagger\Client\TransactionResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countTransactionsByOrganizationUsingGET**](TransactionResourceApi.md#countTransactionsByOrganizationUsingGET) | **GET** /api/transactions/number | countTransactionsByOrganization
[**createTransactionUsingPOST**](TransactionResourceApi.md#createTransactionUsingPOST) | **POST** /api/transactions | createTransaction
[**deleteTransactionUsingDELETE**](TransactionResourceApi.md#deleteTransactionUsingDELETE) | **DELETE** /api/transactions/{id} | deleteTransaction
[**getAllTransactionsUsingGET**](TransactionResourceApi.md#getAllTransactionsUsingGET) | **GET** /api/transactions | getAllTransactions
[**getBeneficiariesByDataFilterDTOUsingPOST1**](TransactionResourceApi.md#getBeneficiariesByDataFilterDTOUsingPOST1) | **POST** /api/transactions/organization | getBeneficiariesByDataFilterDTO
[**getTransactionUsingGET**](TransactionResourceApi.md#getTransactionUsingGET) | **GET** /api/transactions/{id} | getTransaction
[**updateTransactionUsingPUT**](TransactionResourceApi.md#updateTransactionUsingPUT) | **PUT** /api/transactions | updateTransaction


# **countTransactionsByOrganizationUsingGET**
> int countTransactionsByOrganizationUsingGET($organization_id)

countTransactionsByOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_id = 789; // int | organization_id

try {
    $result = $apiInstance->countTransactionsByOrganizationUsingGET($organization_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->countTransactionsByOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | **int**| organization_id |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTransactionUsingPOST**
> \Swagger\Client\Model\Transaction createTransactionUsingPOST($transaction)

createTransaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$transaction = new \Swagger\Client\Model\Transaction(); // \Swagger\Client\Model\Transaction | transaction

try {
    $result = $apiInstance->createTransactionUsingPOST($transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->createTransactionUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction** | [**\Swagger\Client\Model\Transaction**](../Model/Transaction.md)| transaction |

### Return type

[**\Swagger\Client\Model\Transaction**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteTransactionUsingDELETE**
> deleteTransactionUsingDELETE($id)

deleteTransaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteTransactionUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->deleteTransactionUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllTransactionsUsingGET**
> \Swagger\Client\Model\Transaction[] getAllTransactionsUsingGET($organization_id, $programme_id)

getAllTransactions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_id = 789; // int | organization_id
$programme_id = 789; // int | programme_id

try {
    $result = $apiInstance->getAllTransactionsUsingGET($organization_id, $programme_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->getAllTransactionsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | **int**| organization_id | [optional]
 **programme_id** | **int**| programme_id | [optional]

### Return type

[**\Swagger\Client\Model\Transaction[]**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBeneficiariesByDataFilterDTOUsingPOST1**
> \Swagger\Client\Model\Transaction[] getBeneficiariesByDataFilterDTOUsingPOST1($data_filter_dto)

getBeneficiariesByDataFilterDTO

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$data_filter_dto = new \Swagger\Client\Model\DataFilterDTO(); // \Swagger\Client\Model\DataFilterDTO | dataFilterDTO

try {
    $result = $apiInstance->getBeneficiariesByDataFilterDTOUsingPOST1($data_filter_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->getBeneficiariesByDataFilterDTOUsingPOST1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_filter_dto** | [**\Swagger\Client\Model\DataFilterDTO**](../Model/DataFilterDTO.md)| dataFilterDTO |

### Return type

[**\Swagger\Client\Model\Transaction[]**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionUsingGET**
> \Swagger\Client\Model\Transaction getTransactionUsingGET($id)

getTransaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getTransactionUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->getTransactionUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Transaction**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTransactionUsingPUT**
> \Swagger\Client\Model\Transaction updateTransactionUsingPUT($transaction)

updateTransaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TransactionResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$transaction = new \Swagger\Client\Model\Transaction(); // \Swagger\Client\Model\Transaction | transaction

try {
    $result = $apiInstance->updateTransactionUsingPUT($transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionResourceApi->updateTransactionUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction** | [**\Swagger\Client\Model\Transaction**](../Model/Transaction.md)| transaction |

### Return type

[**\Swagger\Client\Model\Transaction**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

