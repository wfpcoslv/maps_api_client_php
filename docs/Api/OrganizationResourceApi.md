# Swagger\Client\OrganizationResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrganizationUsingPOST**](OrganizationResourceApi.md#createOrganizationUsingPOST) | **POST** /api/organizations | createOrganization
[**deleteOrganizationUsingDELETE**](OrganizationResourceApi.md#deleteOrganizationUsingDELETE) | **DELETE** /api/organizations/{id} | deleteOrganization
[**getAllOrganizationsUsingGET**](OrganizationResourceApi.md#getAllOrganizationsUsingGET) | **GET** /api/organizations | getAllOrganizations
[**getOrganizationUsingGET**](OrganizationResourceApi.md#getOrganizationUsingGET) | **GET** /api/organizations/{id} | getOrganization
[**updateOrganizationUsingPUT**](OrganizationResourceApi.md#updateOrganizationUsingPUT) | **PUT** /api/organizations | updateOrganization


# **createOrganizationUsingPOST**
> \Swagger\Client\Model\Organization createOrganizationUsingPOST($organization_dto)

createOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OrganizationResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_dto = new \Swagger\Client\Model\OrganizationDTO(); // \Swagger\Client\Model\OrganizationDTO | organizationDTO

try {
    $result = $apiInstance->createOrganizationUsingPOST($organization_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrganizationResourceApi->createOrganizationUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_dto** | [**\Swagger\Client\Model\OrganizationDTO**](../Model/OrganizationDTO.md)| organizationDTO |

### Return type

[**\Swagger\Client\Model\Organization**](../Model/Organization.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOrganizationUsingDELETE**
> deleteOrganizationUsingDELETE($id)

deleteOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OrganizationResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteOrganizationUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling OrganizationResourceApi->deleteOrganizationUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllOrganizationsUsingGET**
> \Swagger\Client\Model\Organization[] getAllOrganizationsUsingGET()

getAllOrganizations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OrganizationResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllOrganizationsUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrganizationResourceApi->getAllOrganizationsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Organization[]**](../Model/Organization.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOrganizationUsingGET**
> \Swagger\Client\Model\Organization getOrganizationUsingGET($id)

getOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OrganizationResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getOrganizationUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrganizationResourceApi->getOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Organization**](../Model/Organization.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOrganizationUsingPUT**
> \Swagger\Client\Model\Organization updateOrganizationUsingPUT($organization_dto)

updateOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OrganizationResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_dto = new \Swagger\Client\Model\OrganizationDTO(); // \Swagger\Client\Model\OrganizationDTO | organizationDTO

try {
    $result = $apiInstance->updateOrganizationUsingPUT($organization_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrganizationResourceApi->updateOrganizationUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_dto** | [**\Swagger\Client\Model\OrganizationDTO**](../Model/OrganizationDTO.md)| organizationDTO |

### Return type

[**\Swagger\Client\Model\Organization**](../Model/Organization.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

