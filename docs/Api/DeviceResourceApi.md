# Swagger\Client\DeviceResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDeviceUsingPOST**](DeviceResourceApi.md#createDeviceUsingPOST) | **POST** /api/devices | createDevice
[**deleteDeviceUsingDELETE**](DeviceResourceApi.md#deleteDeviceUsingDELETE) | **DELETE** /api/devices/{id} | deleteDevice
[**getAllDevicesUsingGET**](DeviceResourceApi.md#getAllDevicesUsingGET) | **GET** /api/devices | getAllDevices
[**getDeviceUsingGET**](DeviceResourceApi.md#getDeviceUsingGET) | **GET** /api/devices/{id} | getDevice
[**updateDeviceUsingPUT**](DeviceResourceApi.md#updateDeviceUsingPUT) | **PUT** /api/devices | updateDevice


# **createDeviceUsingPOST**
> \Swagger\Client\Model\Device createDeviceUsingPOST($device)

createDevice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device = new \Swagger\Client\Model\Device(); // \Swagger\Client\Model\Device | device

try {
    $result = $apiInstance->createDeviceUsingPOST($device);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceResourceApi->createDeviceUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device** | [**\Swagger\Client\Model\Device**](../Model/Device.md)| device |

### Return type

[**\Swagger\Client\Model\Device**](../Model/Device.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDeviceUsingDELETE**
> deleteDeviceUsingDELETE($id)

deleteDevice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteDeviceUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling DeviceResourceApi->deleteDeviceUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllDevicesUsingGET**
> \Swagger\Client\Model\Device[] getAllDevicesUsingGET()

getAllDevices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllDevicesUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceResourceApi->getAllDevicesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Device[]**](../Model/Device.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDeviceUsingGET**
> \Swagger\Client\Model\Device getDeviceUsingGET($id)

getDevice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getDeviceUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceResourceApi->getDeviceUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Device**](../Model/Device.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDeviceUsingPUT**
> \Swagger\Client\Model\Device updateDeviceUsingPUT($device)

updateDevice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeviceResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device = new \Swagger\Client\Model\Device(); // \Swagger\Client\Model\Device | device

try {
    $result = $apiInstance->updateDeviceUsingPUT($device);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeviceResourceApi->updateDeviceUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device** | [**\Swagger\Client\Model\Device**](../Model/Device.md)| device |

### Return type

[**\Swagger\Client\Model\Device**](../Model/Device.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

