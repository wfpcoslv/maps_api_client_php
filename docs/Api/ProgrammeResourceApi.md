# Swagger\Client\ProgrammeResourceApi

All URIs are relative to *https://maps.wfp.famoco.com/backoffice*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProgrammeUsingPOST**](ProgrammeResourceApi.md#createProgrammeUsingPOST) | **POST** /api/programmes | createProgramme
[**deleteProgrammeUsingDELETE**](ProgrammeResourceApi.md#deleteProgrammeUsingDELETE) | **DELETE** /api/programmes/{id} | deleteProgramme
[**getAllProgrammesUsingGET**](ProgrammeResourceApi.md#getAllProgrammesUsingGET) | **GET** /api/programmes | getAllProgrammes
[**getProgrammeByOrganizationUsingGET**](ProgrammeResourceApi.md#getProgrammeByOrganizationUsingGET) | **GET** /api/programmes/organization/{id} | getProgrammeByOrganization
[**getProgrammeUsingGET**](ProgrammeResourceApi.md#getProgrammeUsingGET) | **GET** /api/programmes/{id} | getProgramme
[**updateProgrammeUsingPUT**](ProgrammeResourceApi.md#updateProgrammeUsingPUT) | **PUT** /api/programmes | updateProgramme


# **createProgrammeUsingPOST**
> \Swagger\Client\Model\Programme createProgrammeUsingPOST($programme)

createProgramme

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProgrammeResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$programme = new \Swagger\Client\Model\Programme(); // \Swagger\Client\Model\Programme | programme

try {
    $result = $apiInstance->createProgrammeUsingPOST($programme);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProgrammeResourceApi->createProgrammeUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **programme** | [**\Swagger\Client\Model\Programme**](../Model/Programme.md)| programme |

### Return type

[**\Swagger\Client\Model\Programme**](../Model/Programme.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProgrammeUsingDELETE**
> deleteProgrammeUsingDELETE($id)

deleteProgramme

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProgrammeResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $apiInstance->deleteProgrammeUsingDELETE($id);
} catch (Exception $e) {
    echo 'Exception when calling ProgrammeResourceApi->deleteProgrammeUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllProgrammesUsingGET**
> \Swagger\Client\Model\Programme[] getAllProgrammesUsingGET()

getAllProgrammes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProgrammeResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllProgrammesUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProgrammeResourceApi->getAllProgrammesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Programme[]**](../Model/Programme.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProgrammeByOrganizationUsingGET**
> \Swagger\Client\Model\Programme[] getProgrammeByOrganizationUsingGET($id)

getProgrammeByOrganization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProgrammeResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getProgrammeByOrganizationUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProgrammeResourceApi->getProgrammeByOrganizationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Programme[]**](../Model/Programme.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProgrammeUsingGET**
> \Swagger\Client\Model\Programme getProgrammeUsingGET($id)

getProgramme

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProgrammeResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 789; // int | id

try {
    $result = $apiInstance->getProgrammeUsingGET($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProgrammeResourceApi->getProgrammeUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id |

### Return type

[**\Swagger\Client\Model\Programme**](../Model/Programme.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProgrammeUsingPUT**
> \Swagger\Client\Model\Programme updateProgrammeUsingPUT($programme)

updateProgramme

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProgrammeResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$programme = new \Swagger\Client\Model\Programme(); // \Swagger\Client\Model\Programme | programme

try {
    $result = $apiInstance->updateProgrammeUsingPUT($programme);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProgrammeResourceApi->updateProgrammeUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **programme** | [**\Swagger\Client\Model\Programme**](../Model/Programme.md)| programme |

### Return type

[**\Swagger\Client\Model\Programme**](../Model/Programme.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

